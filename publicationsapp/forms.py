from django import forms
#from django.forms.extras.widgets import SelectDateWidget
from .models import Article, Author

class ArticleForm(forms.ModelForm):

    class Meta:
        model = Article
        exclude = ["date", "authors"]

class AuthorForm(forms.ModelForm):

    class Meta:
        model = Author
        exclude = [""]

class ArticleUpdateForm(forms.ModelForm):

    class Meta:
        model = Article
        exclude = ["date"]
