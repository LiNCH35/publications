from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import DeleteView
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.urls import reverse_lazy
from .models import Article, Author
from .forms import ArticleForm, AuthorForm, ArticleUpdateForm
import json

def new(request):
    if request.method == "POST":
        data = {
            'title': request.POST["title"],
            'metainfo': {
                'body': request.POST["metainfo"],
            },
        }
        form = ArticleForm(data)
        if form.is_valid():
            new_form = form.save()
    else:
        form = ArticleForm()
    return render(request, 'articles/new.html', locals())

class ArticleDelete(DeleteView):
    model = Article
    success_url = reverse_lazy('all_articles')

    template_name = 'articles/delete.html'

def json_update(request):
    article_id = request.GET.get("article_id")
    article = get_object_or_404(Article, pk=article_id)
    data = serializers.serialize('json', [ article, ])
    return HttpResponse(data)
    # return JsonResponse(data)

def update(request, pk):
    article = get_object_or_404(Article, pk=pk)
    authors = article.authors.all()
    if request.method == 'POST':
        form = ArticleUpdateForm(request.POST)
        if form.is_valid():
            data = {'body': form.cleaned_data['metainfo'],}
            for k, v in dict(request.POST).items():
                v=v[0]
                if k[:5] == 'param' and k.find('name') > 0:
                    data[v] = request.POST[k[:-4]]
            # if ('param3' in request.POST):
            #     data = {
            #         request.POST['param3name']: request.POST['param3'],
            #         "body": form.cleaned_data['metainfo'],
            #     }

            article.title = form.cleaned_data['title']
            article.metainfo = data
            article.authors.set(form.cleaned_data['authors'])
            article.save()
        return render(request, 'articles/new.html', locals())
    else:
        metainfo = article.metainfo.replace('\'', '\"')
        metainfo_js = json.loads(metainfo)
        form = ArticleUpdateForm(initial={
            'title': article,
            'metainfo' : metainfo_js['body'],
            'date' : article.date,
            #'authors' : article.authors
            })
        return render(request, 'articles/new.html', locals())

def show(request, pk):
    article = get_object_or_404(Article, pk=pk)
    metainfo = article.metainfo
    metainfo = json.loads(metainfo.replace('\'', '\"'))
    authors = article.authors.all()
    return render(request, 'articles/post.html', locals())
