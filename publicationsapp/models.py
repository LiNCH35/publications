from django.db import models
from django.contrib.postgres.fields import JSONField
from datetime import datetime

class Author(models.Model):
    firstName = models.CharField(max_length = 50)
    secondName = models.CharField(max_length = 50)
    patronymic = models.CharField(max_length = 50)

    firstName_en = models.CharField(max_length = 50)
    secondName_en = models.CharField(max_length = 50)
    patronymic_en = models.CharField(max_length = 50)

    def __str__(self):
        return u'%s %s. %s.' % (self.secondName, self.firstName[0], self.patronymic[0])

class Article(models.Model):
    title = models.CharField(max_length = 120)
    metainfo = models.TextField()
    #metainfo = JSONField()
    date = models.DateTimeField(default = datetime.now(), blank=True, null=True)
    authors = models.ManyToManyField(Author, blank=True)

    def __str__(self):
        return self.title
