from django.shortcuts import render
from .models import Author, Article
from .forms import ArticleForm, AuthorForm

def new(request):
    form = AuthorForm(request.POST or None)
    if request.method == "POST" and form.is_valid():
        new_form = form.save()
    return render(request, 'authors/new.html', locals())

def show(request, pk):
    author = Author.objects.get(id=pk)
    articles = Article.objects.filter(authors__id = pk)
    return render(request, 'authors/author.html', locals())
