from django.urls import path
from django.views.generic import ListView, DetailView
from .models import Article
from . import views

urlpatterns = [
    path('new/', views.new, name='new_article'),
    path('', ListView.as_view(queryset=Article.objects.all().order_by("-date")[:10]
    , template_name="articles/posts.html"), name='all_articles'),
    path('<slug:pk>/', views.show, name='show_article'),
    path('<slug:pk>/update', views.update, name='update'),
    path('json_update', views.json_update, name='json_update'),
    path('<slug:pk>/delete', views.ArticleDelete.as_view(), name="delete_article"),
    #path('<slug:pk>/', DetailView.as_view(model=Article, template_name="articles/post.html")),
]
