from django.contrib import admin
from .models import Article, Author

class AuthorAdmin(admin.ModelAdmin):

    prepopulated_fields = {'firstName_en': ('firstName',), 'secondName_en': ('secondName',), 'patronymic_en': ('patronymic',)}


admin.site.register(Author, AuthorAdmin)
admin.site.register(Article)
