from django.urls import path
from django.views.generic import ListView, DetailView
from .models import Author
from . import author_views

urlpatterns = [
    path('new/', author_views.new, name='new_author'),
    path('', ListView.as_view(queryset=Author.objects.all().order_by("secondName")[:20]
    , template_name="authors/authors.html")),
    #path('<slug:pk>/', DetailView.as_view(model=Author, template_name="authors/author.html")),
    path('<slug:pk>/', author_views.show, name='show_author'),
]
